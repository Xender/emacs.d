; ELPA repos
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("elpa" . "http://tromey.com/elpa/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")
                        ;("joseito" . "http://joseito.republika.pl/sunrise-commander/")
                        ))

(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

; things from ELPA packages
(package-initialize)
(smart-tabs-insinuate 'c 'c++ 'python)

; Schedule theme load
(add-hook 'after-init-hook '(lambda () (interactive) (load-theme 'monokai)))

;; Keybinds
; Better M-x
(require 'helm)
(require 'helm-config)
(global-set-key (kbd "M-X") 'smex)
(global-set-key [remap execute-extended-command] 'helm-M-x)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(helm-mode 1)

; Move line {down,up}
(global-set-key (kbd "M-<down>") '(lambda ()
                                    (transpose-lines 1)
                                    (interactive)
                                    (forward-line)
                                    (transpose-lines 1)
                                    (forward-line -1)))
(global-set-key (kbd "M-<up>") '(lambda ()
                                    (interactive)
                                    (forward-line -2)))

(global-undo-tree-mode 1)
;(ido-everywhere-mode)

(require 'multiple-cursors)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

(global-set-key (kbd "C-w") '(lambda (ARG)
                               (interactive "p")
                               (if mark-active
				   (call-interactively 'kill-region)
				 (backward-kill-word ARG))))

; misc settings
(setq org-hide-leading-stars t)

; Custom scripts
(add-to-list 'load-path "~/.emacs.d/my")
(require 'c-c++-header)
(put 'dired-find-alternate-file 'disabled nil)


(setq mouse-autoselect-window t)
(show-paren-mode 1)
(setq show-paren-delay 0)
(setq c-default-style "k&r"
      c-basic-offset 4
      c-basic-indent 4
      tab-width 4
      default-tab-width 4
      indent-tabs-mode t)

(require 'use-package)
(use-package nlinum
  :init (global-nlinum-mode 1))
(use-package company
  :config (progn
	    (setq company-idle-delay 0.25
		  company-clang-arguments '("-std=c++11"))
	    (global-company-mode 1)))

(use-package highlight-symbol
  :bind (("<f10>"   . highlight-symbol-at-point)
         ("<C-f10>" . highlight-symbol-remove-all)
         ("C-c w"   . highlight-symbol-at-point)
         ("C-c W"   . highlight-symbol-remove-all)
         ("<f11>"   . highlight-symbol-prev)
         ("M-p"     . highlight-symbol-prev)
         ("<f12>"   . highlight-symbol-next)
         ("M-n"     . highlight-symbol-next)))

(use-package autopair
  :init (progn
	  (define-globalized-minor-mode global-autopair-mode autopair-mode
	    (lambda () (autopair-mode 1)))
	  (global-autopair-mode 1)))

(use-package neotree
  :bind ("C-c b" . neotree-toggle))

(use-package tabbar)

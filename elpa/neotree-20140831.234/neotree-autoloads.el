;;; neotree-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (neotree neotree-dir neotree-hide neotree-show
;;;;;;  neotree-toggle neotree-find) "neotree" "neotree.el" (21515
;;;;;;  1612 900983 124000))
;;; Generated autoloads from neotree.el

(autoload 'neotree-find "neotree" "\
Quick select node which specified PATH in NeoTree.
If path is nil and no buffer file name, then use DEFAULT-PATH,

\(fn &optional PATH DEFAULT-PATH)" t nil)

(autoload 'neotree-toggle "neotree" "\
Toggle show the NeoTree window.

\(fn)" t nil)

(autoload 'neotree-show "neotree" "\
Show the NeoTree widnow.

\(fn)" t nil)

(autoload 'neotree-hide "neotree" "\
Close the NeoTree window.

\(fn)" t nil)

(autoload 'neotree-dir "neotree" "\
Show the NeoTree window, and change root to PATH.

\(fn PATH)" t nil)

(autoload 'neotree "neotree" "\
Show the NeoTree window.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("neotree-pkg.el") (21515 1613 29673 569000))

;;;***

(provide 'neotree-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; neotree-autoloads.el ends here
